-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 10, 2019 at 02:50 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `recipedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `ingredientlist`
--

CREATE TABLE `ingredientlist` (
  `ingredients` int(11) NOT NULL,
  `ingredientname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ingredientlist`
--

INSERT INTO `ingredientlist` (`ingredients`, `ingredientname`) VALUES
(1, 'chicken'),
(2, 'sausage'),
(3, 'bay leaf');

-- --------------------------------------------------------

--
-- Table structure for table `recipes`
--

CREATE TABLE `recipes` (
  `recipename` varchar(255) NOT NULL,
  `ingredients` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `directions` mediumtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recipes`
--

INSERT INTO `recipes` (`recipename`, `ingredients`, `photo`, `directions`) VALUES
('Chicken Noodle Soup', '1, 4, 5', 'kirby.jpg', 'Combine broth and seasoning in a large pot and bring to a boil. Stir in celery and onion. Reduce heat and simmer for 15 minutes. Mix cornstarch and water together in a small bowl and add to soup to thicken. Stir in chicken and noodles. Cook until chicken is cooked through and noodles are soft.\r\n'),
('Chicken Tortilla Soup', '6,7,8', 'kirby.jpg', 'Saute onion and garlic in oil in a medium stock pot until soft. Stir in chili powder, oregano, tomatoes, broth, and water. Bring to a boil and simmer for 5 to 10 minutes. Stir in corn, hominy, chiles, beans, cilantro, and chicken. Simmer for 10 minutes.Serve with tortilla chips, avocado slices, cheese, and chopped green onions.'),
('gumbo', '1,2,3', 'GUMBO.jpg', 'make roux');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ingredientlist`
--
ALTER TABLE `ingredientlist`
  ADD PRIMARY KEY (`ingredients`);

--
-- Indexes for table `recipes`
--
ALTER TABLE `recipes`
  ADD PRIMARY KEY (`recipename`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ingredientlist`
--
ALTER TABLE `ingredientlist`
  MODIFY `ingredients` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
