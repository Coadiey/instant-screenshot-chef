from app.models import allIngredientsFromRecipesQuery, ingredientNameToIngredientNumber, makefilter
import pytest

# test_back_end.py contains all unit tests for backend code


# The following tests the recipes table to
# verify if the recipename is in the recipes table
# and if the allIngredientFromRecipesQuery function
# works properly
@pytest.fixture
def allFromRecipes():
    return allIngredientsFromRecipesQuery()

@pytest.mark.parametrize("index, expected", [
    (0, "Chicken Noodle Soup"),
    (1, "Chicken Tortilla Soup"),
    (2, "gumbo"),
])

def test_recipes_recipename(allFromRecipes, index, expected):
    assert allFromRecipes[index].recipename.lower() == expected.lower()

# The following tests the ingredientlist table to verify
# if the ingredientname matches the expected ingredientnumber
# and the ingredientNameToIngredientNumber function works properly
@pytest.mark.parametrize("ingredientName, expected", [
    ("chicken", '1'),
    ('sausage', '2'),
    ('bay leaf', '3'),
])

def test_recipes_ingredients(ingredientName, expected):
    assert ingredientNameToIngredientNumber(ingredientName) == expected

# The following tests the makeFilter function; Which checks whether the user's ingredients
# are used in any recipes and if they are it returns that recipe in a List containing Named_tuples
# The Name_tuples containt callable names which correspond to all the columns from the recipes
# table in the recipedb database
@pytest.mark.parametrize("index, ingredientNum1, ingredientNum2, ingredientNum3, expected", [
    (0, '1', '2', '3', 'gumbo'),
])

def test_make_filter(allFromRecipes, index, ingredientNum1, ingredientNum2, ingredientNum3, expected):
    filter123 = makefilter(ingredientNum1, ingredientNum2, ingredientNum3)
    assert list(filter(filter123, allFromRecipes))[index].recipename.lower() == expected.lower()