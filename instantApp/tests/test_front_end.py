from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
import pytest

driver = webdriver.Firefox(executable_path='C:\\Users\Coadiey\Documents\CMPS-453_Intro_to_Software_Methodology\Homework\instant-screenshot-chef\instantApp\drivers\geckodriver.exe')

driver.set_page_load_timeout(20)
driver.get("https://zambiey.pythonanywhere.com/")
time.sleep(6)
driver.find_element_by_id("DrpDwnMn0-5s31label").click()
ingredient1 = driver.find_element_by_name("ingredient1")
ingredient1.send_keys('Chicken')
ingredient1.send_keys(Keys.ENTER)
# def test_search():
    # assert  result == 'Cool'
time.sleep(4)
currentURL = str(driver.current_url)
def test_url_after_ingredient_search():	
	assert currentURL  == 'https://zambiey.pythonanywhere.com/recipes/'
driver.quit()

print("Test completed successfully")
