import mysql.connector

def dbConnection():
	mydb = mysql.connector.connect(
    host="Zambiey.mysql.pythonanywhere-services.com",
    user="Zambiey",
    password="CoadieyBryan",
    database = "Zambiey$recipedb"
    )
	return mydb

def allIngredientsFromRecipesQuery():
	mydb = dbConnection()
	mycursor = mydb.cursor(named_tuple=True)
	mycursor.execute("SELECT * FROM recipes;")
	myresult = mycursor.fetchall()
	return myresult


# String -> String
# Translates the ingredient name into corresponding
# ingredient number, as designated by database. Then
# returns resulting number as string
def ingredientNameToIngredientNumber(ingredient):
	mydb = dbConnection()
	mycursor = mydb.cursor(named_tuple=True)
	mycursor.execute("""
	SELECT
		ingredients
	FROM
		ingredientlist
	WHERE
		ingredientname = %(ingredient)s;
		""", {
			'ingredient': ingredient
	})
	resultingArray = mycursor.fetchone()
	try:
		resultInt = resultingArray.ingredients
		resultStr = str(resultInt)
	except Exception as e:
		resultStr = False
	else:
		pass
	return resultStr

# Number Number Number -> List<Named_tuples>
# Closure used to pass the arguments to the checkIngredients while
# still letting it be used by filter
def makefilter(ingredient1, ingredient2, ingredient3):
	# Named_Tuple(string,string,string) -> List<Named_tuples>
	# Checks if the given row from a Named_tuple contains
	# the desired ingredients and if it does it returns that
	# row from Named_tuple in the output list
	def checkIngredients(row):
		str = row.ingredients
		arrayOfStr = str.split(',')
		return all([ingredient1 in arrayOfStr, ingredient2 in arrayOfStr, ingredient3 in arrayOfStr])
	return checkIngredients

# FYI: A Closure is a function object that remembers values in enclosing scopes
# even if they are not present in memory.