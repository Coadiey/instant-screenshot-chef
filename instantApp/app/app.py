from flask import Flask, render_template, request
import flask
from models import allIngredientsFromRecipesQuery, ingredientNameToIngredientNumber, makefilter

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('homepage.html')

@app.route('/ingredients/')
def ingredients():
    return render_template('ingredientspage.html')

@app.route('/recipes/', methods = ['POST', 'GET'])
def recipes():
    allOfRecipes = allIngredientsFromRecipesQuery()
    # If user got to Recipes page from ingredients page
    # Search for recipes that use their ingredients
    if flask.request.method == 'POST':
        # Assigns user's requested ingredients to variables
        ingredienta = request.form['ingredient1']
        ingredientb = request.form['ingredient2']
        ingredientc = request.form['ingredient3']
        # Gets the corresponding "number" for requested ingredient
        # and assigns each to a variable
        ingredient1NumStr = ingredientNameToIngredientNumber(ingredienta)
        ingredient2NumStr = ingredientNameToIngredientNumber(ingredientb)
        ingredient3NumStr = ingredientNameToIngredientNumber(ingredientc)
        #handle the case that the database doesn't have ingredients or they are blank
        if not all([ingredient1NumStr, ingredient2NumStr, ingredient3NumStr]):
            listIngredients = [ingredient1NumStr, ingredient2NumStr, ingredient3NumStr]
            possibleIngredients = list(filter(None ,listIngredients))
            if possibleIngredients:
                for i in listIngredients:
                    if not i in possibleIngredients:
                        i = possibleIngredients[0]
            else:
                recipesIWant = allOfRecipes
        # Makes a closure function object that passes the ingredients as
        # variables to nested function
        filter123 = makefilter(ingredient1NumStr, ingredient2NumStr, ingredient3NumStr)
        # Applies the filter123 function to all rows of allOfRecipes tuple
        # This will return a list containing all recipes using all of user's ingredients
        recipesIWant = list(filter(filter123, allOfRecipes))
    # If user got here by selecting the Recipes page in navigation bar
    else:
        # Return all recipes in database
        recipesIWant = allOfRecipes
    return render_template('recipespage.html', recipesIWant = recipesIWant)

@app.route('/contact/')
def contact():
    return render_template('contactpage.html')

if __name__ == '__main__':
    app.run(debug=True)